package com.carrefour.kata.controller;

import com.carrefour.kata.dto.DeliveryDTO;
import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/deliveries")
public class DeliveryController {
  private final DeliveryService deliveryService;

  @Autowired
  public DeliveryController(DeliveryService deliveryService) {
    this.deliveryService = deliveryService;
  }

  @GetMapping
  public List<DeliveryDTO> getAllDeliveries() {
    return deliveryService.getAllDeliveries();
  }

  @GetMapping("/{deliveryId}")
  public DeliveryDTO getDeliveryById(@PathVariable Long deliveryId) {
    return deliveryService.getDeliveryById(deliveryId);
  }

  @PostMapping
  public DeliveryDTO createDelivery(@RequestBody DeliveryDTO deliveryDTO) {
    return deliveryService.createDelivery(deliveryDTO);
  }

  @DeleteMapping("/{deliveryId}")
  public void deleteDelivery(@PathVariable Long deliveryId) {
    deliveryService.deleteDelivery(deliveryId);
  }

  @GetMapping("/types")
  public List<DeliveryType> getDeliveryType(){
    return Arrays.stream(DeliveryType.values()).toList();
  }
}
