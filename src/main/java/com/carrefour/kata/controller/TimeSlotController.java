package com.carrefour.kata.controller;

import com.carrefour.kata.dto.Day;
import com.carrefour.kata.dto.TimeSlotsResponse;
import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.service.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/timeslots")
public class TimeSlotController {
  private final TimeSlotService timeSlotService;

  @Autowired
  public TimeSlotController(TimeSlotService timeSlotService) {
    this.timeSlotService = timeSlotService;
  }

  @GetMapping("/days")
  public List<Day> getDays() {
    return timeSlotService.getNextFiveDays();
  }

  @PostMapping
  public TimeSlotsResponse getAllTimeSlots(Day day, DeliveryType mode) {
    return timeSlotService.calculateAvailableSlots(day, mode);
  }

}
