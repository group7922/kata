package com.carrefour.kata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryDTO {
  private String DeliveryId;
  private String clientId;
  private String    deliveryType;
  private LocalDate deliveryDate;
  private TimeSlotDTO timeSlot;
}
