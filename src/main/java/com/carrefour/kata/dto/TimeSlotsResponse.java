package com.carrefour.kata.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class TimeSlotsResponse {
	private String message;
	private LocalDate date;
	private List<TimeSlotDTO> timeSlotDTOList;
}
