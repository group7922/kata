package com.carrefour.kata.mapper;

import com.carrefour.kata.dto.DeliveryDTO;
import com.carrefour.kata.model.Delivery;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface DeliveryMapper {

	DeliveryDTO deliveryToDeliveryDTO(Delivery delivery);
	Delivery deliveryDTOToDelivery(DeliveryDTO deliveryDTO);
}
