package com.carrefour.kata.mapper;

import com.carrefour.kata.dto.TimeSlotDTO;
import com.carrefour.kata.model.TimeSlot;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TimeSlotMapper {

	TimeSlot timeSlotDTOTOTimeSlot(TimeSlotDTO timeSlotDTO);
	TimeSlotDTO timeSlotTOTimeSlotDTO(TimeSlot timeSlot);
}
