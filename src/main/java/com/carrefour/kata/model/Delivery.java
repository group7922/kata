package com.carrefour.kata.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "deliveries")
public class Delivery {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;
  private DeliveryType deliveryType;
  private LocalDate deliveryDate;
  private String clientId;
  @ManyToOne
  @JoinColumn(name = "time_slot_id")
  private TimeSlot  timeSlot;
}
