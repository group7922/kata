package com.carrefour.kata.model;

public enum DeliveryType {
	DRIVE,
	DELIVERY,
	DELIVERY_TODAY,
	DELIVERY_ASAP
}
