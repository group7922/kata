package com.carrefour.kata.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "timeslots")
public class TimeSlot {

  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  private DeliveryType deliveryMode;

  @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
  private LocalTime startTime;

  @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
  private LocalTime endTime;

  // Static method to obtain a builder instance
  public static Builder builder() {
    return new Builder();
  }

  // Builder class
  public static class Builder {
    private Long id;
    private DeliveryType deliveryMode;
    private LocalTime startTime;
    private LocalTime endTime;

    public Builder id(Long id) {
      this.id = id;
      return this;
    }

    public Builder deliveryMode(DeliveryType deliveryMode) {
      this.deliveryMode = deliveryMode;
      return this;
    }

    public Builder startTime(LocalTime startTime) {
      this.startTime = startTime;
      return this;
    }

    public Builder endTime(LocalTime endTime) {
      this.endTime = endTime;
      return this;
    }

    public TimeSlot build() {
      TimeSlot timeSlot = new TimeSlot();
      timeSlot.id = this.id;
      timeSlot.deliveryMode = this.deliveryMode;
      timeSlot.startTime = this.startTime;
      timeSlot.endTime = this.endTime;
      return timeSlot;
    }
  }
}
