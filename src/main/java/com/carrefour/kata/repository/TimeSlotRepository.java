package com.carrefour.kata.repository;

import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.model.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {

  List<TimeSlot> findByDeliveryMode(DeliveryType deliveryMode);

}
