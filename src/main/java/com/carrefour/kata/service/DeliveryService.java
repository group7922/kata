package com.carrefour.kata.service;

import com.carrefour.kata.dto.DeliveryDTO;

import java.util.List;

public interface DeliveryService {
  List<DeliveryDTO> getAllDeliveries();
  DeliveryDTO getDeliveryById(Long deliveryId);
  DeliveryDTO createDelivery(DeliveryDTO deliveryDTO);
  void deleteDelivery(Long deliveryId);
}
