package com.carrefour.kata.service.Impl;

import com.carrefour.kata.dto.DeliveryDTO;
import com.carrefour.kata.mapper.DeliveryMapper;
import com.carrefour.kata.mapper.TimeSlotMapper;
import com.carrefour.kata.model.Delivery;
import com.carrefour.kata.model.TimeSlot;
import com.carrefour.kata.repository.DeliveryRepository;
import com.carrefour.kata.repository.TimeSlotRepository;
import com.carrefour.kata.service.DeliveryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeliveryServiceImpl implements DeliveryService {

  private final DeliveryMapper deliveryMapper;
  private final TimeSlotMapper timeSlotMapper;
  private final DeliveryRepository deliveryRepository;
  private final TimeSlotRepository timeSlotRepository;

  public DeliveryServiceImpl(TimeSlotRepository timeSlotRepository,  DeliveryRepository deliveryRepository, DeliveryMapper deliveryMapper, TimeSlotMapper timeSlotMapper) {
    this.timeSlotRepository = timeSlotRepository;
    this.deliveryRepository = deliveryRepository;
    this.deliveryMapper = deliveryMapper;
    this.timeSlotMapper = timeSlotMapper;
  }


  @Override
  public List<DeliveryDTO> getAllDeliveries() {
    List<Delivery> deliveries = deliveryRepository.findAll();
    return deliveries.stream().map( deliveryMapper::deliveryToDeliveryDTO).toList();
  }

  @Override
  public DeliveryDTO getDeliveryById(Long deliveryId) {
    Delivery delivery = deliveryRepository.getReferenceById(deliveryId);
    return deliveryMapper.deliveryToDeliveryDTO(delivery);
  }

  @Override
  public DeliveryDTO createDelivery(DeliveryDTO deliveryDTO) {
    Delivery delivery = deliveryMapper.deliveryDTOToDelivery(deliveryDTO);
    TimeSlot timeSlot = timeSlotMapper.timeSlotDTOTOTimeSlot(deliveryDTO.getTimeSlot());
    TimeSlot savedTimeSlot = timeSlotRepository.save(timeSlot);
    delivery.setTimeSlot(savedTimeSlot);
    Delivery instertedDelivery = deliveryRepository.save(delivery);
    return deliveryMapper.deliveryToDeliveryDTO(instertedDelivery);
  }

  @Override
  public void deleteDelivery(Long deliveryId) {
    Delivery delivery = deliveryRepository.getReferenceById(deliveryId);
    deliveryRepository.delete(delivery);
  }
}
