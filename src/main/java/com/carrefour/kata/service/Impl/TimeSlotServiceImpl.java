package com.carrefour.kata.service.Impl;

import com.carrefour.kata.dto.Day;
import com.carrefour.kata.dto.TimeSlotDTO;
import com.carrefour.kata.dto.TimeSlotsResponse;
import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.service.TimeSlotService;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class TimeSlotServiceImpl implements TimeSlotService {

  public TimeSlotsResponse calculateAvailableSlots(Day day,  DeliveryType mode) {
    TimeSlotsResponse response = new TimeSlotsResponse();
    List<TimeSlotDTO> availableSlots = new ArrayList<>();

    LocalTime startTime = getStartTimeForMode(mode);
    LocalTime endTime = getEndTimeForMode(mode);
    int duration = getDurationForMode(mode);

    if(mode.equals(DeliveryType.DELIVERY_ASAP) && LocalTime.now().isAfter(LocalTime.of(20, 0))){
      while (startTime.plusMinutes(duration).isBefore(endTime) || startTime.plusMinutes(duration).equals(endTime)) {
        TimeSlotDTO timeSlotDTO = TimeSlotDTO.builder().build();
        timeSlotDTO.setStartTime(String.valueOf(startTime));
        timeSlotDTO.setEndTime(String.valueOf(startTime));
        availableSlots.add(timeSlotDTO);
      }
      response.setMessage("Aucun crénau disponible pour aujourd'hui, voici les crénaux disponible pour demain");
      response.setDate(LocalDate.now());
    } else if (("Today".equalsIgnoreCase(day.getDayName()) && LocalTime.now().isBefore(LocalTime.of(18, 0) ) ) || (mode.equals(DeliveryType.DELIVERY_TODAY) &&  LocalTime.now().isBefore(LocalTime.of(20, 0) )) ){
      startTime = adjustStartTimeForToday(startTime);
      while (startTime.plusMinutes(duration).isBefore(endTime) || startTime.plusMinutes(duration).equals(endTime)) {
        TimeSlotDTO timeSlotDTO = TimeSlotDTO.builder().build();
        timeSlotDTO.setStartTime(String.valueOf(truncateToMinutes(startTime)));
        startTime = startTime.plusMinutes(duration);
        timeSlotDTO.setEndTime(String.valueOf(truncateToMinutes(startTime)));
        availableSlots.add(timeSlotDTO);
      }
      response.setMessage("Voici les créneaux disponibles pour aujourd'hui");
      response.setDate(LocalDate.now());
    } else {
      while (startTime.plusMinutes(duration).isBefore(endTime) || startTime.plusMinutes(duration).equals(endTime)) {
        TimeSlotDTO timeSlotDTO = TimeSlotDTO.builder().build();
        timeSlotDTO.setStartTime(String.valueOf(truncateToMinutes(startTime)));
        startTime = startTime.plusMinutes(duration);
        timeSlotDTO.setEndTime(String.valueOf(truncateToMinutes(startTime)));
        availableSlots.add(timeSlotDTO);
      }
      if(mode.equals(DeliveryType.DELIVERY_TODAY )|| "today".equalsIgnoreCase(day.getDayName()) ) {
        response.setMessage("Plus aucun créneau aujourd'hui, voici les créneaux pour demain.");
        response.setDate(LocalDate.now().plusDays(1));
      } else {
        response.setMessage("Les créneaux disponible pour"+day.getDate());
        response.setDate(day.getDate());
      }
      response.setTimeSlotDTOList(availableSlots);
    }
    return response;
  }

  private LocalTime adjustStartTimeForToday(LocalTime startTime) {
    LocalTime now = LocalTime.now();
    return (now.isAfter(startTime) ? now.plusMinutes((60 - now.getMinute()) % 30) : startTime);
  }

  private LocalTime getStartTimeForMode(DeliveryType mode) {
    switch (mode) {
      case DELIVERY_TODAY, DRIVE:
        return LocalTime.now().plus(Duration.ofHours(2));
      case DELIVERY_ASAP:
        return LocalTime.now().plus(Duration.ofMinutes(30));
      default:
        return LocalTime.of(8, 0);
    }
  }

  private LocalTime getEndTimeForMode(DeliveryType mode) {
    if(mode.equals(DeliveryType.DRIVE)) {
      return LocalTime.of(21, 0);
    }
    return LocalTime.of(20, 0);
  }

  private int getDurationForMode(DeliveryType mode) {
    switch(mode) {
      case DELIVERY_ASAP -> {return 30; }
      case DELIVERY_TODAY, DELIVERY -> {
        return 120;
      }
      default ->  {return 60;}
    }
  }

  public List<Day> getNextFiveDays() {
    List<Day> days = new ArrayList<>();
    LocalDate today = LocalDate.now();

    for (int i = 0; i < 5; i++) {
      Day day = Day.builder().build();
      LocalDate nextDay = today.plusDays(i);
      String dayName = getDayName(i, nextDay);
      day.setDayName(dayName); day.setDate(nextDay);
      days.add(day);
    }

    return days;
  }

  private String getDayName(int dayIndex, LocalDate date) {
    if (dayIndex == 0) {
      return "Today";
    } else if (dayIndex == 1) {
      return "Tomorrow";
    } else {
      return date.getDayOfWeek().name();
    }
  }

  public static LocalTime truncateToMinutes(LocalTime time) {
    return time.withSecond(0).withNano(0);
  }
}
