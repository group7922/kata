package com.carrefour.kata.service;

import com.carrefour.kata.dto.Day;
import com.carrefour.kata.dto.TimeSlotsResponse;
import com.carrefour.kata.model.DeliveryType;

import java.util.List;

public interface TimeSlotService {
    public TimeSlotsResponse calculateAvailableSlots(Day date, DeliveryType mode);
    public List<Day> getNextFiveDays();
}
