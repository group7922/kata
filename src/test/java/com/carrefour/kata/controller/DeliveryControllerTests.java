package com.carrefour.kata.controller;


import com.carrefour.kata.dto.DeliveryDTO;
import com.carrefour.kata.dto.TimeSlotDTO;
import com.carrefour.kata.mapper.DeliveryMapper;
import com.carrefour.kata.model.Delivery;
import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.service.Impl.DeliveryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DeliveryController.class)
public class DeliveryControllerTests {

	@Spy
	private DeliveryMapper deliveryMapper= Mappers.getMapper(DeliveryMapper.class);


	@MockBean
	DeliveryServiceImpl deliveryService;

	@MockBean
	CommandLineRunner demoData;

	@Autowired
	ObjectMapper objectMapper = new ObjectMapper();

	DeliveryDTO deliveryDTO1;
	DeliveryDTO deliveryDTO2;
	DeliveryDTO requestDTO;
	TimeSlotDTO timeSlot;

	@Autowired
	private MockMvc mockMvc;

	@Before
	public void SetUp() {
		timeSlot = TimeSlotDTO.builder().startTime(LocalTime.now().toString()).endTime(LocalTime.now().plusMinutes(120).toString()).build();
		deliveryDTO1 = DeliveryDTO.builder().clientId("1").deliveryDate(LocalDate.now()).deliveryType(String.valueOf(DeliveryType.DELIVERY)).timeSlot(timeSlot).build();
		deliveryDTO2 = DeliveryDTO.builder().clientId("4").deliveryDate(LocalDate.now()).deliveryType(String.valueOf(DeliveryType.DRIVE)).timeSlot(timeSlot).build();

		requestDTO = DeliveryDTO.builder().clientId("1").deliveryDate(LocalDate.parse(LocalDate.now().toString())).deliveryType(String.valueOf(DeliveryType.DELIVERY)).timeSlot(timeSlot).build();

	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void it_should_return_created_delivery() throws Throwable {
		Delivery request = deliveryMapper.deliveryDTOToDelivery(requestDTO);
		when(deliveryService.createDelivery(any(DeliveryDTO.class))).thenReturn(deliveryDTO1);

		mockMvc.perform(post("/deliveries").content(objectMapper.writeValueAsString(request))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.deliveryType").value(request.getDeliveryType().toString()));
	}

}
