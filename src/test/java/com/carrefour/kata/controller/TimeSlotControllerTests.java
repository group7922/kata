package com.carrefour.kata.controller;


import com.carrefour.kata.dto.Day;
import com.carrefour.kata.service.Impl.TimeSlotServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TimeSlotController.class)
public class TimeSlotControllerTests {
	@MockBean
	TimeSlotServiceImpl timeSlotService;

	@MockBean
	CommandLineRunner demoData;

	ObjectMapper objectMapper = new ObjectMapper();

	Day day1;
	Day day2;
	List<Day> list;

	@Autowired
	private MockMvc mockMvc;

	@Before
	public void SetUp() {

	}

	@Test
	public void it_should_return_created_order() throws Throwable {
		when(timeSlotService.getNextFiveDays()).thenReturn(list);

		mockMvc.perform(get("/timeslots/days"))
				.andExpect(status().isOk());
	}

}
