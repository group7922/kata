package com.carrefour.kata.mapper;

import com.carrefour.kata.dto.TimeSlotDTO;
import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.model.TimeSlot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class TimeSlotMapperImplTest {
	private TimeSlotMapperImpl timeSlotMapper;

	TimeSlotDTO timeSlotDTO1;
	TimeSlotDTO timeSlotDTO2;
	TimeSlot timeSlot1;
	TimeSlot timeSlot2;

	@Before
	public void setUp() {
		timeSlotMapper = new TimeSlotMapperImpl();

		timeSlotDTO1 = TimeSlotDTO.builder()
				.startTime(String.valueOf(LocalTime.now().withSecond(0).withNano(0)))
				.endTime(String.valueOf(LocalTime.now().plusMinutes(120).withSecond(0).withNano(0)))
				.build();
		timeSlotDTO2 = TimeSlotDTO.builder()
				.startTime(String.valueOf(LocalTime.now().plusMinutes(30).withSecond(0).withNano(0)))
				.endTime(String.valueOf(LocalTime.now().plusMinutes(60).withSecond(0).withNano(0)))
				.build();
		timeSlot1 = TimeSlot.builder()
				.deliveryMode(DeliveryType.DELIVERY)
				.startTime(LocalTime.of(9, 0).withSecond(0).withNano(0))
				.endTime(LocalTime.of(20, 0).withSecond(0).withNano(0))
				.build();
		timeSlot2 = TimeSlot.builder()
				.deliveryMode(DeliveryType.DRIVE)
				.startTime(LocalTime.of(9, 0).withSecond(0).withNano(0))
				.endTime(LocalTime.of(22, 0).withSecond(0).withNano(0))
				.build();
	}

	@Test
	public void timeSlotDTO_to_timeSlot_test() {
		TimeSlot timeSlot = timeSlotMapper.timeSlotDTOTOTimeSlot(timeSlotDTO1);
		assertThat(timeSlot).isNotNull();
		assertThat(timeSlot.getStartTime()).isEqualTo(timeSlotDTO1.getStartTime());
		assertThat(timeSlotMapper.timeSlotTOTimeSlotDTO(null)).isNull();
	}

	@Test
	public void timeSlot_to_timeSlotDTO_test() {
		TimeSlotDTO timeSlotDTO = timeSlotMapper.timeSlotTOTimeSlotDTO(timeSlot1);
		LocalTime time = LocalTime.parse(timeSlotDTO.getEndTime()).withSecond(0).withNano(0);
		assertThat(time).isEqualTo(timeSlot1.getEndTime());
		assertThat(timeSlotMapper.timeSlotTOTimeSlotDTO(null)).isNull();
	}

}