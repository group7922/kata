package com.carrefour.kata.repository;

import com.carrefour.kata.model.DeliveryType;
import com.carrefour.kata.model.TimeSlot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TimeSlotRepositoryTests {
  @Autowired
  private TimeSlotRepository timeSlotRepository;

  private TimeSlot timeSlotToCreate1;
  private TimeSlot timeSlotToCreate2;
  private TimeSlot timeSlotCreated1;
  private TimeSlot timeSlotCreated2;

  @Before
  public void setUp() {

    timeSlotToCreate1 = TimeSlot.builder()
            .startTime(LocalTime.now())
            .endTime(LocalTime.now().plusMinutes(120))
            .deliveryMode(DeliveryType.DELIVERY)
            .build();

    timeSlotToCreate2 = TimeSlot.builder()
            .startTime(LocalTime.now().plusMinutes(30))
            .endTime(LocalTime.now().plusMinutes(60))
            .deliveryMode(DeliveryType.DRIVE)
            .build();
  }

  @Test
  public void save_client_test() {
    timeSlotCreated1 = timeSlotRepository.save(timeSlotToCreate1);
    timeSlotCreated2 = timeSlotRepository.save(timeSlotToCreate2);

    assertThat(timeSlotCreated1).isNotNull();
    assertThat(timeSlotCreated2).isNotNull();

    Optional<TimeSlot> timeSlot1 = timeSlotRepository.findById(1L);
    Optional<TimeSlot> timeSlot2 = timeSlotRepository.findById(2L);

    assertThat(timeSlot1).isNotNull();
    assertThat(timeSlot2).isNotNull();
    assertThat(timeSlot2.get().getId()).isEqualTo(timeSlotCreated2.getId());
  }

  @Test
  public void findAllClients_test() {
    timeSlotCreated1 = timeSlotRepository.save(timeSlotToCreate1);
    timeSlotCreated2 = timeSlotRepository.save(timeSlotToCreate2);

    assertThat(timeSlotCreated1).isNotNull();
    assertThat(timeSlotCreated2).isNotNull();


    List<TimeSlot> allTimeSlots = timeSlotRepository.findAll();

    assertThat(allTimeSlots).isNotNull()
            .isNotEmpty();
  }
}
